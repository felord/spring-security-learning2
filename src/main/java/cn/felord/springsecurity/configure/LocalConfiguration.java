package cn.felord.springsecurity.configure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.nio.charset.StandardCharsets;
import java.util.Locale;

/**
 * i18n支持
 *
 * @author felord.cn
 * @since 1.0.0
 */
@Configuration
public class LocalConfiguration {

    /**
     * 默认解析器 其中locale表示默认语言
     *
     * @return the locale resolver
     */
    @Bean
    public LocaleResolver defaultLocaleResolver() {
        SessionLocaleResolver localeResolver = new SessionLocaleResolver();
        localeResolver.setDefaultLocale(Locale.CHINA);
        return localeResolver;
    }


    /**
     * Reloadable resource bundle message source reloadable resource bundle message source.
     *
     * @return the reloadable resource bundle message source
     */
    @Bean
    ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource(){
        ReloadableResourceBundleMessageSource source = new ReloadableResourceBundleMessageSource();
        source.setDefaultEncoding(StandardCharsets.UTF_8.name());
        source.setBasename("classpath:org/springframework/security/messages");
        return source;
    }

}
