package cn.felord.springsecurity.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

/**
 * 拿来测试
 */
@RestController
@RequestMapping("/foo")
public class FooController {

    @GetMapping("/hello")
    public Map<String, String> hello() {
        return Collections.singletonMap("hello", "world");
    }

}
